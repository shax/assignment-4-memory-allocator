//
// Created by shax on 17.12.22.
//
#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include <sys/mman.h>

#define HEAP_SIZE  8175
#define BLOCK_SIZE 512
#define TEST_COUNT 5

typedef bool (*test)(void);


static void heap_free(size_t sz, void *heap){
    munmap(heap, size_from_capacity( (block_capacity){.bytes=sz}).bytes);
}

static struct block_header* get_header(void* block) {
    return (struct block_header*) ((uint8_t *)block - offsetof(struct block_header, contents));
}



static void print_error(int64_t num){
    fprintf(stderr, "Тест %ld провален \n", num);
}

static void print_success_test(int64_t num){
    fprintf(stdout, "Тест %ld прошел \n", num);
}

static void print_start_test(int64_t num){
    fprintf(stdout, "Тест %ld стартовал \n", num);
}

bool start_test1(){
    printf("проверка работы метода");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap== NULL) {
        return false;
    }
    void* block = _malloc(1024);
    if (block== NULL) return false;
    debug_heap(stdout, heap);
    _free(block);
    heap_free(HEAP_SIZE, heap);
    return true;
}

bool start_test2(){
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout,heap);
    void *first_block = _malloc(BLOCK_SIZE);
    void *second_block = _malloc(BLOCK_SIZE);
    struct block_header *second_block_header = get_header(second_block);
    void *third_block = _malloc(BLOCK_SIZE);
    if (!heap || !first_block || !second_block || !third_block){
        return false;
    }
    debug_heap(stdout, heap);
    _free(second_block);
    debug_heap(stdout, heap);
    if (!second_block_header->is_free){
        return false;
    }
    _free(first_block);
    _free(third_block);
    heap_free(HEAP_SIZE, heap);
    return true;
}

bool start_test3(){
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *first_block = _malloc(BLOCK_SIZE);
  //  struct block_header *first_block_header = get_header(first_block);
    void *second_block = _malloc(BLOCK_SIZE);
   // struct block_header *second_block_header = get_header(second_block);
    void *third_block = _malloc(BLOCK_SIZE);
   // struct block_header *third_block_header = get_header(third_block);
    if (!heap || !first_block || !second_block || !third_block){
        return false;
    }
    debug_heap(stdout, heap);
    _free(third_block);
    _free(second_block);
    debug_heap(stdout, heap);
    _free(first_block);
    heap_free(HEAP_SIZE, heap);
    return true;
}

bool start_test4(){
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *first_block = _malloc(8192);
    void *second_block = _malloc(8192);
    struct block_header *first_block_header = get_header(first_block);
    struct block_header *second_block_header = get_header(second_block);
    if (heap== NULL || first_block==NULL || second_block==NULL || first_block_header->next != second_block_header){
        return false;
    }
    debug_heap(stdout, heap);
    _free(first_block);
    _free(second_block);
    debug_heap(stdout, heap);
    heap_free(8192, heap);
    return true;
}

bool start_test5(){
    void *heap = heap_init(HEAP_SIZE);
    void *first_block = _malloc(HEAP_SIZE);
    struct block_header *first_block_header = get_header(first_block);
    debug_heap(stdout, heap);
    void* after =  first_block_header->capacity.bytes + (void *)first_block_header->contents;
    void * map_region =mmap(after,
                            REGION_MIN_SIZE,
                            PROT_READ | PROT_WRITE,
                            MAP_PRIVATE | MAP_FIXED,
                            -1,
                            0);
    if (map_region == MAP_FAILED){
        return false;
    }
    void *second_block = _malloc(BLOCK_SIZE);
    struct block_header *second_block_header = get_header(second_block);
    debug_heap(stdout, heap);
    if (second_block == NULL || second_block_header == (void *) first_block_header->contents + first_block_header->capacity.bytes || second_block_header->capacity.bytes != BLOCK_SIZE ){
        heap_free(HEAP_SIZE, heap);
        munmap(map_region, HEAP_SIZE);
        return false;
    }
    heap_free(HEAP_SIZE, heap);
    munmap(map_region, HEAP_SIZE);
    return true;

}

test all_tests[]={
        start_test1,
        start_test2,
        start_test3,
        start_test4,
        start_test5,
};



void start_all_test(){
    for (int64_t i=0; i<TEST_COUNT; i++){
        print_start_test(i+1);
        if (all_tests[i]()){
            print_success_test(i+1);
        }else{
            print_error(i+1);
        }
    }
}