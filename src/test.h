//
// Created by shax on 17.12.22.
//

#ifndef MEMORY_ALLOCATOR_TEST_H
#define MEMORY_ALLOCATOR_TEST_H

#include <stdbool.h>

bool start_test1();
bool start_test2();
bool start_test3();
bool start_test4();
bool start_test5();
void start_all_test();

#endif //MEMORY_ALLOCATOR_TEST_H
